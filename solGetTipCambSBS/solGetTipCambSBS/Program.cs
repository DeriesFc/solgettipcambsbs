﻿using Entity;
using Logic.app;
using System;
using System.Collections.Generic;
using WS;
using WS.MODEL_RES;

namespace app
{
    public class Program
    {
        TIPCBO TC = null;
        modTipCamb o = null;
        IList<entTipCamb> data = null;

        static void Main(string[] args) {
            Console.Title = "Exchange Rate";
            Program P = new Program();
        }
        
        public Program()
        {
            try
            {
                Console.WriteLine("Iniciando getTipCambio SBS");
                Console.WriteLine("Esperar");
                TC = new WS.TIPCBO();
                getData(); //IList<entTipCamb> data
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex);
                throw;
            }
        }

        #region GetData
        public async void getData()
        {
            Console.WriteLine("Captando información...");

            DateTime fecha = DateTime.Today.AddDays(-1);

            o = await TC.GetDataSBS(fecha.ToString("dd-MM-yyyy"));
            data = adapter(o, fecha.ToString("MM-dd-yyyy"));

            logTipCamb q = new logTipCamb();
            data = q.addAll(data);

            print();

            Console.WriteLine("\nFinalizo, Cerrar...");
            Environment.Exit(0);
        }
        public IList<entTipCamb> adapter(modTipCamb obj, string fecha) {
            IList<entTipCamb> rpt;
            try
            {
                rpt = new List<entTipCamb>();
                rpt.Add(new entTipCamb(1, fecha, convert(o.compDolNA), convert(o.ventDolNA)));
                rpt.Add(new entTipCamb(2, fecha, convert(o.compDolNC), convert(o.ventDolNC)));
                rpt.Add(new entTipCamb(3, fecha, convert(o.compLibEst), convert(o.ventLibEst)));
                rpt.Add(new entTipCamb(4, fecha, convert(o.compYenJap), convert(o.ventYenJap)));
                rpt.Add(new entTipCamb(5, fecha, convert(o.compFraSui), convert(o.ventFraSui)));
                rpt.Add(new entTipCamb(6, fecha, convert(o.compEuro), convert(o.ventEuro)));
                rpt.Add(new entTipCamb(7, fecha, convert(o.compPesMex), convert(o.ventPesMex)));
            }
            catch (Exception)
            {
                rpt = null;
                throw;
            }
            return rpt;
        }
        public Decimal convert(string obj) {
            Decimal result = obj.Equals("") ? 0.0M : Decimal.Parse(obj);
            return result;
        }
        #endregion

        #region printData
        public void print() {
            Console.WriteLine("\nResultados...");
            Console.WriteLine($"Moneda \t Fecha \t\t Compra  Venta ");
            foreach (var obj in data)
            {
                Console.WriteLine($"{obj.id_moneda} \t {obj.fecha} \t {obj.compra} \t {obj.venta} ");
            }
        }
        #endregion
    }
}                
                 
                 
                 
                
                
                
                 
                 
                 
                 