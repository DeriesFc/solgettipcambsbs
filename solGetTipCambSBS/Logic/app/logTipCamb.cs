﻿using DataAccess;
using Entity;
using System;
using System.Collections.Generic;

namespace Logic.app
{
    public class logTipCamb : iLogic<entTipCamb>
    {
        private entOrigen db = _listCnx.oList[0];

        public int add(entTipCamb obj)
        {
            try
            {
                using (iFace<entTipCamb> ida = new datTipCamb(db))
                {
                    obj.id = ida.add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return obj.id;
        }

        public IList<entTipCamb> addAll(IList<entTipCamb> list)
        {
            try
            {
                using (iFace<entTipCamb> ida = new datTipCamb(db))
                {
                    foreach (entTipCamb obj in list) obj.id = ida.add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }


        public int upd(entTipCamb obj)
        {
            int result = -1;
            try
            {
                using (iFace<entTipCamb> ida = new datTipCamb(db))
                {
                    result = ida.upd(obj);
                }
            }
            catch
            {
                result = -1;
                throw;
            }
            return result;
        }

        public int del(string obj)
        {
            int result = -1;
            try
            {
                using (iFace<entTipCamb> ida = new datTipCamb(db))
                {
                    result = ida.del(obj);
                }
            }
            catch (Exception)
            {
                result = -1;
                throw;
            }
            return result;
        }

        public IList<entTipCamb> list()
        {
            try
            {
                using (iFace<entTipCamb> result = new datTipCamb(db))
                    return result.list();
            }
            catch
            {
                return new List<entTipCamb>();
                throw;
            }
        }

        public IList<entTipCamb> listObj(entTipCamb obj)
        {
            try
            {
                using (iFace<entTipCamb> result = new datTipCamb(db))
                    return result.listObj(obj);
            }
            catch
            {
                return new List<entTipCamb>();
                throw;
            }
        }


        public entTipCamb sel(string obj)
        {
            try
            {
                using (iFace<entTipCamb> result = new datTipCamb(db))
                    return result.sel(obj);
            }
            catch
            {
                return new entTipCamb();
                throw;
            }
        }

        public entTipCamb selObj(entTipCamb obj)
        {
            try
            {
                using (iFace<entTipCamb> result = new datTipCamb(db))
                    return result.selObj(obj);
            }
            catch
            {
                return new entTipCamb();
                throw;
            }
        }
    }
}
