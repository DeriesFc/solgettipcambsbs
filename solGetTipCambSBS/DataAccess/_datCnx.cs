﻿using System;
using DataAccess.lib;
using Entity;

namespace DataAccess
{
    public static class _datCnx
    {
        #region Variables de la Conexión
        public static GenSQL _GenSQL;
        #endregion

        #region Manejo de la Conexión
        public static bool Iniciar(entOrigen obj)
        {
            bool booRslt = true;
            try
            {
                switch (obj.motor.id)
                {
                    case 0:
                        _GenSQL = new _MySQL(obj.svr, obj.port, obj.db, obj.user, obj.pass);
                        break;
                    case 1:
                        _GenSQL = new _MsSQL(obj.svr, obj.db, obj.user, obj.pass);
                        break;
                }

                booRslt = _GenSQL.Autenticar();

                if (!booRslt)
                {
                    throw new Exception("Error al iniciar Sesión de Conexión con la Base de Datos." +
                        Environment.NewLine + " Verificar parámetros de conexión." +
                        Environment.NewLine + " Verificar conexión 1con el servidor." +
                        Environment.NewLine + " Verificar conexión de red." +
                        Environment.NewLine + " cnx.Iniciar()");
                }
                else
                {
                    System.Diagnostics.Debug.Print("Conectado a base de datos");
                }

            }
            catch (Exception ex)
            {
                booRslt = false;
                throw ex;
            }
            return booRslt;
        }

        public static void Finalizar()
        {
            try
            {
                if (_GenSQL != null) _GenSQL.CerrarConexion();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void IniciarTransaccion()
        {
            _GenSQL.IniciarTransaccion();
        }

        public static void TerminarTransaccion()
        {
            _GenSQL.TerminarTransaccion();
        }

        public static void AbortarTransaccion()
        {
            _GenSQL.AbortarTransaccion();
        }
        #endregion
    }
}
