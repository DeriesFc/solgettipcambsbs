﻿using Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class datTipCamb : _datBase, iFace<entTipCamb>
    {
        public datTipCamb(entOrigen obj) : base(obj){}

        public int add(entTipCamb _obj)
        {
            try { return _datCnx._GenSQL.Ejecutar("setTipCamb", _obj.id_moneda, DateTime.Parse(_obj.fecha), _obj.compra, _obj.venta); }
            catch (Exception ex)
            {
                return 0;
                throw ex;
            }
        }

        public int upd(entTipCamb _obj)
        {
            try
            {
                return _datCnx._GenSQL.Ejecutar("", "");
            }
            catch (Exception ex)
            {
                return 0;
                throw ex;
            }
        }

        public int del(string _obj)
        {
            try
            {
                return _datCnx._GenSQL.Ejecutar("", "");
            }
            catch (Exception ex)
            {
                return 0;
                throw ex;
            }
        }

        public entTipCamb sel(string _obj)
        {
            entTipCamb obj = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader) _datCnx._GenSQL.TraerDataReader("", _obj))
                {
                    while (dr.Read()) obj = new entTipCamb(dr);
                }
            }
            catch (Exception ex)
            {
                obj = new entTipCamb();
                throw ex;
            }
            return obj;
        }

        public entTipCamb selObj(entTipCamb _obj)
        {
            entTipCamb obj = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader) _datCnx._GenSQL.TraerDataReader("", _obj.id))
                {
                    while (dr.Read()) obj = new entTipCamb(dr);
                }
            }
            catch (Exception ex)
            {
                obj = new entTipCamb();
                throw ex;
            }
            return obj;
        }

        public IList<entTipCamb> list()
        {
            IList<entTipCamb> oList = null;
            try
            {
                entTipCamb obj = null;
                oList = new List<entTipCamb>();
                using (SqlDataReader dr = (SqlDataReader) _datCnx._GenSQL.TraerDataReader(""))
                {
                    while (dr.Read())
                    {
                        obj = new entTipCamb(dr);
                        oList.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<entTipCamb>();
                throw ex;
            }
            return oList;
        }

        public IList<entTipCamb> listObj(entTipCamb _obj)
        {
            IList<entTipCamb> oList = null;
            try
            {
                entTipCamb obj = null;
                oList = new List<entTipCamb>();
                using (SqlDataReader dr = (SqlDataReader) _datCnx._GenSQL.TraerDataReader("", _obj.id))
                {
                    while (dr.Read())
                    {
                        obj = new entTipCamb(dr);
                        oList.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<entTipCamb>();
                throw ex;
            }
            return oList;
        }

    }
}
