﻿using System.Collections.Generic;
using Entity;

namespace DataAccess
{
    public static class _listCnx
    {
        public static  IList<entOrigen> oList = toList();

        private static IList<entOrigen> toList()
        {
            IList<entOrigen> oList = new List<entOrigen>();
            oList.Add(new entOrigen("localhost", "127.0.0.1", "3306", "db_utils", "root", "Passw0rd", _motor.oList[0]));
            oList.Add(new entOrigen("localhost", "127.0.0.1", "3306", "db_utils", "root", "", _motor.oList[0]));
            return oList;
        }
    }

    public static class _motor
    {
        public static IList<entMotor> oList = toList();
        private static IList<entMotor> toList()
        {
            IList<entMotor> oList = new List<entMotor>();
            oList.Add(new entMotor("MySQL"));
            oList.Add(new entMotor("MsSQL"));
            return oList;
        }
    }



}
