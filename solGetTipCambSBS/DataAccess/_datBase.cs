﻿using System;
using Entity;
namespace DataAccess
{
    public class _datBase : IDisposable
    {
        public _datBase(entOrigen obj)
        {
            try
            {
                bool booCnx = _datCnx.Iniciar(obj);
                if (!booCnx)
                {
                    Dispose();
                    throw new Exception("Error al Iniciar conexión a DB => [datBase]");
                }

            }
            catch (Exception ex) { throw (ex); }
        }

        ~_datBase()
        {
            try { Dispose(); }
            catch (Exception ex) { throw ex; }
        }

        public void Dispose()
        {
            try
            {
                _datCnx.Finalizar();
                GC.SuppressFinalize(this);
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
