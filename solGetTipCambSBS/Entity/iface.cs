﻿using System;
using System.Collections.Generic;

namespace Entity
{
    public interface iFace<T> : IDisposable
    {
        int add(T _obj);
        int upd(T _obj);
        int del(string _obj);
        T sel(string _obj);
        T selObj(T _obj);
        IList<T> list();
        IList<T> listObj(T _obj);
    }

    public interface iLogic<T>
    {
        int add(T _obj);
        int upd(T _obj);
        int del(string _obj);
        T sel(string _obj);
        T selObj(T _obj);
        IList<T> list();
        IList<T> listObj(T _obj);
    }

}
