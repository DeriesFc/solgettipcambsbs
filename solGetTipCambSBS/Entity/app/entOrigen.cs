﻿namespace Entity
{
    public class entOrigen
    {
        #region Variables
        public int id { set; get; } = 0;
        public string nom { set; get; } = string.Empty;
        public string svr { set; get; } = string.Empty;
        public string port { set; get; } = string.Empty;
        public string db { set; get; } = string.Empty;
        public string user { set; get; } = string.Empty;
        public string pass { set; get; } = string.Empty;
        public entMotor motor { set; get; } = null;
        #endregion

        #region Constructor
        public entOrigen() {}

        public entOrigen(string _nom, string _svr, string _port, string _db, string _user, string _pass, entMotor _motor)
        {
            nom = _nom;
            svr = _svr;
            port = _port;
            db = _db;
            user = _user;
            pass = _pass;
            motor = _motor;
        }

        public entOrigen(int _id, string _nom, string _svr, string _port, string _db, string _user, string _pass, entMotor _motor){
            id = _id;
            nom = _nom;
            svr = _svr;
            port = _port;
            db = _db;
            user = _user;
            pass = _pass;
            motor = _motor;
        }
        #endregion
    }

    public class entMotor
    {
        #region Variables
        public int id { set; get; } = 0;
        public string nom { set; get; } = string.Empty;
        #endregion

        #region Constructor
        public entMotor() { }
        public entMotor(string _nom) { nom = _nom; }
        public entMotor(int _id, string _nom) { id = _id; nom = _nom; }
        #endregion

    }

}
