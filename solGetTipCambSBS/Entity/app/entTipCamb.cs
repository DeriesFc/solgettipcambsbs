﻿using System.Data;
using System.Runtime.Serialization;

namespace Entity
{
    public class entTipCamb
    {
        #region Propiedad
        public int id { get; set; } = 0;
        public int id_moneda { get; set; } = 0;
        public string fecha { get; set; } = string.Empty;
        public decimal compra { get; set; } = 0.0m;
        public decimal venta { get; set; } = 0.0m;
        #endregion

        #region Constructor
        public entTipCamb() { }

        public entTipCamb(int _id_moneda, string _fecha, decimal _compra, decimal _venta)
        {
            id_moneda = _id_moneda;
            fecha = _fecha;
            compra = _compra;
            venta = _venta;
        }

        public entTipCamb(int _id, int _id_moneda, string _fecha, decimal _compra, decimal _venta)
        {
            id = _id;
            id_moneda = _id_moneda;
            fecha = _fecha;
            compra = _compra;
            venta = _venta;
        }

        public entTipCamb(IDataReader dr)
        {
            try
            {
                id = dr.GetInt32(dr.GetOrdinal("id"));
                id_moneda = dr.GetInt32(dr.GetOrdinal("id_moneda"));
                fecha = dr.GetString(dr.GetOrdinal("fecha"));
                compra = dr.GetDecimal(dr.GetOrdinal("compra"));
                venta = dr.GetDecimal(dr.GetOrdinal("venta"));
            }
            catch (System.Exception) { throw; }
        }
        #endregion

        #region Metodo
        public override string ToString()
        {
            return $"{id}, {id_moneda}, {fecha}, {compra}, {venta}";
        }
        #endregion
    }
}
